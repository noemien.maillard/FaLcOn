# FaLcOn

## Name
Fasta Local reComposition based On haplotypes (FaLcOn)

## Description
FaLcOn is useful to generate fasta files with snp and/or indels mutations given reference sequence and table files with positions to mutate, ref/alt alleles and individuals haplotypes.

**Python version 3.10+**

## Usage
```python
# generate fasta file including SNPs only
python3 falcon.py -r ref_seq.fa -m 'snp' -o output_prefix --snp-table snp_mutation_table.csv

# generate fasta file including indels only
python3 falcon.py -r ref_seq.fa -m 'indels' -o output_prefix --indels-table indels_mutation_table.csv

# generate fasta file including both SNPs and indels
python3 falcon.py -r ref_seq.fa -m 'all' -o output_prefix --snp-mutation-table snp_mutation_table.csv --indels-mutation-table indels_mutation_table.csv
```

Additional informations:
- mutation_table is csv format from excel export so it consider ';' as separator (not ',').

## Help
usage: falcon.py [-h] [-r REF_SEQ] [-o OUTPUT] [-m MUTATION_TYPE] [--snp-table SNP_TABLE] [--indels-table INDELS_TABLE] [-s SEP]

options:

&emsp;-h, --help

&emsp;&emsp;&emsp;Show this help message and exit

&emsp;-r REF_SEQ, --ref-seq REF_SEQ

&emsp;&emsp;&emsp;Reference fasta file.

&emsp;-o OUTPUT, --output OUTPUT

&emsp;&emsp;&emsp;Output prefix including path for fasta filenames.

&emsp;-m MUTATION_TYPE, --mutation-type MUTATION_TYPE

&emsp;&emsp;&emsp;The type of mutation. "all", "snp" and "indels" values are accepted.

&emsp;--snp-mutation-table SNP_TABLE

&emsp;&emsp;&emsp;Table containing SNPs. Columns of the table: "POS, REF, ALT, [HAP1, ..., HAPN]" with HAP, the haplotype of each inidividual.  

&emsp;--indels-mutation-table INDELS_TABLE

&emsp;&emsp;&emsp;Table containing indels. Columns of the table: "POS, REF, ALT, [HAP1, ..., HAPN]" with HAP, the haplotype of each inidividual.

&emsp;-s SEP, --sep SEP

&emsp;&emsp;&emsp;Mutation tables separator. Default to ";" (excel csv output).

## Support
For support, contact noemien.maillard@inrae.fr

## Project status
Done.
