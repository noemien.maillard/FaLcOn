refseq.fa: reference sequence
snp and indels.csv files: snp and indels tables
checker* files: handmade mutated sequences
design_check_falcon.svg: file used for handmade mutations
run* files: mutated fasta sequences runned with FaLcOn
