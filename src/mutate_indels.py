# Creation date: 2023/10/12
# Last review: 2023/10/13
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: EAGLE
# Script aim: Mutate indels function.


def mutate_indels(ref_seq, pos_list, mutation_table):
    seq = ''
    # list of positions to mutate
    mut_tab_pos = mutation_table['POS'].to_list()
    # nt to ignore caused by a deletion
    del_len = 0
    for nt, pos in zip(ref_seq, pos_list):
        # ignore deleted nt
        if del_len > 0:
            del_len -= 1
            continue

        # increment positions not to be mutated
        if pos not in mut_tab_pos:
            seq += nt
            continue
        
        # store haplotype (0 for REF or 1 for ALT)
        haplo = mutation_table.iloc[:,3].loc[mutation_table['POS']==pos].iloc[0]

        # update sequence according to haplotype (case 0 or case 1)
        match haplo:
            case 0:
                # if ref => keep nt from ref
                seq += nt
            case 1:
                # update deletion length only if it is a deletion for the given haplotype
                del_len = len(mutation_table['REF'].loc[mutation_table['POS']==pos].iloc[0])-1
                seq += mutation_table['ALT'].loc[mutation_table['POS']==pos].iloc[0]

    return seq
