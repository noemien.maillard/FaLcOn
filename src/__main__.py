# Creation date: 2023/10/12
# Last review: 2023/10/27
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: EAGLE
# Script aim: Main FaLcOn file, take options and run script.


import argparse
import pandas as pd
import re
import sys

from .mutate_snp import mutate_snp
from .mutate_indels import mutate_indels


def get_options():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-r',
        '--ref-seq',
        help='Reference fasta file.'
    )
    parser.add_argument(
        '-m',
        '--mutations-type',
        help='The type of mutation. "snp", "indels" and "all" values are accepted. \
            For "all" value, the script assumes columns of haplotype tables are the same and in the same order.'
    )
    parser.add_argument(
        '-o',
        '--output',
        help='Output prefix including path for fasta filenames.'
    )
    parser.add_argument(
        '-s',
        '--snp-mutation-table',
        help='Table containing SNP. Columns of the table: "POS, REF, ALT, [HAP1, ..., HAPN]" \
            with HAPN haplotype of each inidividual.'
    )
    parser.add_argument(
        '-i',
        '--indels-mutation-table',
        help='Table contaning indels. Columns of the table : "POS, REF, ALT, [HAP1, ..., HAPN]" \
            with HAPN haplotype of each inidividual.'
    )
    parser.add_argument(
        '--sep',
        default=';',
        help='Mutation tables separator. Default to ";".'
    )
    return parser.parse_args()


def main():
    # argument parser
    args = get_options()

    # arguments checkpoint
    if args.ref_seq is None:
        sys.exit('Need a fasta sequence as reference file.\nExit code: 1')
    if args.mutations_type not in ['snp', 'indels', 'all']:
        sys.exit('Argument "mutations-type" must be snp or indels.\nExit code: 1')
    if not args.output:
        sys.exit("Missing output prefix.\nExit code: 1")
    if args.mutations_type in ['snp', 'all'] and args.snp_mutation_table is None:
        sys.exit('SNP mutation table needed.\nExit code: 1')
    if args.mutations_type in ['indels', 'all'] and args.indels_mutation_table is None:
        sys.exit('Indels mutation table needed.\nExit code: 1')

    # extract start and stop from first line and store sequence
    fasta = open(args.ref_seq, 'r').read().splitlines()
    interval = re.search('[0-9]{1,10}-[0-9]{1,10}', fasta[0])
    start, stop = interval.group().split('-')
    start, stop = int(start), int(stop)
    # store sequence as a unique str
    ref_seq = "".join(fasta[1:])

    # read SNP mutation table then check input format
    snp_mutation_table = pd.read_csv(args.snp_mutation_table, sep=args.sep, engine='python')
    if snp_mutation_table.columns.to_list()[:3] != ['POS', 'REF', 'ALT']:
        sys.exit('SNP mutation table columns must start with "POS, REF, ALT" columns and follow with haplotypes columns.\nExit code: 1')
    # separate info columns and haplotypes columns
    snp_info_table = snp_mutation_table.iloc[:,:3]
    snp_haplo_table = snp_mutation_table.iloc[:,3:]

    # read indels mutation table then check input format
    indels_mutation_table = pd.read_csv(args.indels_mutation_table, sep=args.sep, engine='python')
    if indels_mutation_table.columns.to_list()[:3] != ['POS', 'REF', 'ALT']:
        sys.exit('Indels mutation table columns must start with "POS, REF, ALT" columns and follow with haplotypes columns.\nExit code: 1')
    # separate info columns and haplotypes columns
    indels_info_table = indels_mutation_table.iloc[:,:3]
    indels_haplo_table = indels_mutation_table.iloc[:,3:]

    # list all positions from start to stop
    pos_list = list(range(start, stop))

    match args.mutations_type:
        case 'snp':
            for col in snp_haplo_table:
                # mutate sequence
                seq = mutate_snp(ref_seq=ref_seq, pos_list=pos_list, mutation_table=snp_info_table.join(snp_haplo_table[col]))
                # write mutated sequence
                with open(f'{args.output}_{col}_snp.fa', 'w') as output:
                    output.write(f'>Haplotype_snp_{col} | refseq start-stop: {start}-{stop}\n{seq}\n')

        case 'indels':
            for col in indels_haplo_table:
                # mutate sequence
                seq = mutate_indels(ref_seq=ref_seq, pos_list=pos_list, mutation_table=indels_info_table.join(indels_haplo_table[col]))
                # write mutated sequence
                with open(f'{args.output}_{col}.fa', 'w') as output:
                    output.write(f'>Haplotype_{col} | refseq start-stop: {start}-{stop}\n{seq}\n')

        case 'all':
            for col in snp_haplo_table:
                seq = mutate_snp(ref_seq=ref_seq, pos_list=pos_list, mutation_table=snp_info_table.join(snp_haplo_table[col]))
                seq = mutate_indels(ref_seq=seq, pos_list=pos_list, mutation_table=indels_info_table.join(indels_haplo_table[col]))
                with open(f'{args.output}_{col}.fa', 'w') as output:
                    output.write(f'>Haplotype_{col} | refseq start-stop: {start}-{stop}\n{seq}\n')
    