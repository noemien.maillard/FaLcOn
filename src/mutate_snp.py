# Creation date: 2023/10/12
# Last review: 2023/10/13
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: EAGLE
# Script aim: Mutate snp function.


def mutate_snp(ref_seq, pos_list, mutation_table):
    seq = ''
    # list of positions to mutate
    mut_tab_pos = mutation_table['POS'].to_list()
    for nt, pos in zip(ref_seq, pos_list):
        # increment positions not to be mutated
        if pos not in mut_tab_pos:
            seq += nt
            continue
        # store haplotype (0 for REF or 1 for ALT)
        haplo = mutation_table.iloc[:,3].loc[mutation_table['POS']==pos].iloc[0]
        # update sequence according to haplotype (case 0 or case 1)
        match haplo:
            case 0:
                seq += mutation_table['REF'].loc[mutation_table['POS']==pos].iloc[0]
            case 1:
                seq += mutation_table['ALT'].loc[mutation_table['POS']==pos].iloc[0]

    return seq
